#include <QRegExp>
#include <QFile>
#include <QDebug>
#include "utfscriptmodel.h"

utfScriptModel::utfScriptModel()
{
    rows = 0;
    positions = NULL;
    r_tra = new QRegExp("<(\\d{4})>\\s*(.+)");
    r_ori = new QRegExp("//\\s*<(\\d{4})>\\s*(.+)");
    r_com = new QRegExp("//(.+)");
}

utfScriptModel::utfScriptModel(const QString &file)
{
    utfScriptModel();
    openScript(file);
}

utfScriptModel::~utfScriptModel()
{
    closeScript();
}

void utfScriptModel::openScript(const QString &file)
{
    QFile fl(file, this);
    QTextStream str;

    if(rows)
        closeScript();

    fl.open(QIODevice::ReadWrite | QIODevice::Text);
    str.setDevice(&fl);
    str.setCodec("UTF-8");
    str.seek(0);

    // let's hog some memory, baby!
    while(!str.atEnd())
        lines.append(str.readLine());
    fl.close();
    calcPositions();

    // we don't know how many rows we're going to insert after actually inserting them
    beginInsertRows(QModelIndex(), 0, rows-1);
    endInsertRows();
}

void utfScriptModel::closeScript()
{
    beginRemoveRows(QModelIndex(), 0, rows-1);
    rows = 0;
    lines.clear();
    if(positions != NULL)
        delete [] positions;
    positions = NULL;
    endRemoveRows();
}

// updates int rows and int *positions
void utfScriptModel::calcPositions()
{
    if(positions != NULL)
        delete [] positions;

    // lines.size() is actually way too much, but at least we can be
    // sure it's enough without having to calculate the actual size
    positions = new int[lines.size()+1];

    rows = 0;
    int i;
    for(i=0; i<lines.size(); i++)
        if(r_tra->exactMatch(lines[i]))
            positions[rows++] = i;
    positions[rows] = i;
}

QStringList utfScriptModel::lineInfo(const int line) const
{
    QStringList nfo;
    nfo << "" << "" << "" << ""; // num, orig, trans, comm
    for(int i = positions[line]; i < positions[line+1]; i++) {
        if(nfo[0] == "" && r_tra->exactMatch(lines[i])) {
            nfo[0] = r_tra->cap(1);
            nfo[2] = r_tra->cap(2);
        } else if(nfo[1] == "" && r_ori->exactMatch(lines[i])) {
            nfo[1] = r_ori->cap(2);
            if(nfo[1] == nfo[2])
                nfo[2] = "";
        } else if(r_com->exactMatch(lines[i])) {
            if(nfo[3] != "")
                nfo[3] += "\n";
            nfo[3] += r_com->cap(1);
        } else
            break;
    }
    return nfo;
}



int utfScriptModel::rowCount(const QModelIndex &parent) const
{
    return rows;
}

int utfScriptModel::columnCount(const QModelIndex &parent) const
{
    return 4; // #line, original, translation, comments
}

QVariant utfScriptModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid() || index.row() >= rows || index.column() > 3 || role != Qt::DisplayRole)
        return QVariant();

    return lineInfo(index.row()).at(index.column());
}

QVariant utfScriptModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation == Qt::Vertical || role != Qt::DisplayRole)
        return QVariant();
    return QString(section == 0 ? "Line" : section == 1 ? "Original" : section == 2 ? "Translation" : "Comments");
}
