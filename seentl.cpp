#include "seentl.h"
#include "ui_seentl.h"
#include <QHeaderView>
#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>

SEENTL::SEENTL(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::SEENTLClass)
{
    ui->setupUi(this);

    // some settings to make the lines table look good
    ui->lines->verticalHeader()->setDefaultSectionSize(ui->lines->fontMetrics().height()+1);
    ui->lines->horizontalHeader()->setVisible(0);
    ui->lines->verticalHeader()->setVisible(0);
    ui->lines->setModel(&script);
    ui->lines->horizontalHeader()->setResizeMode(0, QHeaderView::ResizeToContents);
    ui->lines->horizontalHeader()->setResizeMode(1, QHeaderView::Stretch);
    ui->lines->horizontalHeader()->setResizeMode(2, QHeaderView::Stretch);
    ui->lines->horizontalHeader()->setResizeMode(3, QHeaderView::Fixed);
    ui->lines->horizontalHeader()->resizeSection(3, 100);

    connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(openScript()));
    connect(ui->actionClose, SIGNAL(triggered()), this, SLOT(closeScript()));
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(closeApp()));
    connect(ui->lines->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(selectLine(QItemSelection,QItemSelection)));
}

SEENTL::~SEENTL()
{
    delete ui;
}

void SEENTL::openScript()
{
    closeScript();
    QString fn = QFileDialog::getOpenFileName(this, "Open File", "", "Script files (*.utf)");
    if(fn == "")
        return;
    QFileInfo fi(fn);
    if(!fi.exists() || !fi.isFile() || !fi.isReadable() || !fi.isWritable()) {
        QMessageBox::critical(this, "Can't open file", "File either doesn't exist or isn't readable and writable");
        return;
    }
    script.openScript(fn);
    file = fi;

    setWindowTitle(QString("%1 - SEENTL").arg(file.fileName()));
    ui->actionClose->setEnabled(1);
    ui->statusBar->showMessage(QString("Found %1 translatable lines").arg(script.rowCount(QModelIndex())), 2000);
}

void SEENTL::closeScript()
{
    if(file.fileName() == "")
        return;
    // TODO; ask if opened script has changes
    script.closeScript();
    file.setFile("");
    ui->actionClose->setEnabled(0);
    setWindowTitle(QString("SEENTL"));
}

void SEENTL::closeApp()
{
    closeScript();
    close();
}

void SEENTL::selectLine(const QItemSelection &selected, const QItemSelection &deselected)
{
    int row = selected.indexes().at(0).row();
    if(row >= script.rowCount(QModelIndex()))
        return;
    QStringList nfo = script.lineInfo(row);
    ui->lineNr->setText(QString("<%1>").arg(nfo[0]));
    ui->lineOrig->setText(nfo[1]);
    ui->lineTrans->setText(nfo[2]);
    ui->lineComm->clear();
    ui->lineComm->appendPlainText(nfo[3]);
    ui->lineTrans->setFocus();
}
