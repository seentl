#ifndef UTFSCRIPTMODEL_H
#define UTFSCRIPTMODEL_H

#include <QAbstractTableModel>
#include <QStringList>

class utfScriptModel : public QAbstractTableModel
{
public:
    utfScriptModel();
    utfScriptModel(const QString&);
    ~utfScriptModel();
    void openScript(const QString&);
    void closeScript();
    QStringList lineInfo(const int line) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

private:
    QStringList lines;
    int rows, *positions;
    QRegExp *r_tra, *r_ori, *r_com;

    void calcPositions();
};

#endif // UTFSCRIPTMODEL_H
