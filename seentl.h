#ifndef SEENTL_H
#define SEENTL_H

#include <QtGui/QMainWindow>
#include <QFileInfo>
#include <QItemSelection>
#include "utfscriptmodel.h"

namespace Ui
{
    class SEENTLClass;
}

class SEENTL : public QMainWindow
{
    Q_OBJECT

public:
    SEENTL(QWidget *parent = 0);
    ~SEENTL();

public slots:
    void openScript();
    void closeScript();
    void closeApp();
    void selectLine(const QItemSelection &selected, const QItemSelection &deselected);

private:
    Ui::SEENTLClass *ui;
    utfScriptModel script;
    QFileInfo file;

};

#endif // SEENTL_H
